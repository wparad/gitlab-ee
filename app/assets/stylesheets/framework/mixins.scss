/**
 * Prefilled mixins
 * Mixins with fixed values
 */

@mixin str-truncated($max_width: 82%) {
  display: inline-block;
  overflow: hidden;
  text-overflow: ellipsis;
  vertical-align: top;
  white-space: nowrap;
  max-width: $max_width;
}

/*
 * Mixin for markdown tables
 */
@mixin markdown-table {
  width: auto;
  display: inline-block;
  overflow-x: auto;
  border: 0;
  border-color: $md-area-border;

  @supports(width: fit-content) {
    display: block;
    width: fit-content;
  }

  tr {
    th {
      border-bottom: solid 2px $md-area-border;
    }

    td {
      border-color: $md-area-border;
    }
  }
}

/*
 * Base mixin for lists in GitLab
 */
@mixin basic-list {
  margin: 5px 0;
  padding: 0;
  list-style: none;

  > li {
    @include clearfix;

    padding: 10px 0;
    border-bottom: 1px solid $list-border-light;
    display: block;
    margin: 0;

    &:last-child {
      border-bottom: 0;
    }

    &.active {
      background: $gray-light;

      a {
        font-weight: $gl-font-weight-bold;
      }
    }

    &.hide {
      display: none;
    }

    &.light {
      a {
        color: $gl-text-color;
      }
    }
  }
}

@mixin basic-list-stats {
  .stats {
    float: right;
    line-height: $list-text-height;
    color: $gl-text-color;

    span {
      margin-right: 15px;
    }
  }
}

@mixin bulleted-list {
  > ul {
    list-style-type: disc;

    ul {
      list-style-type: circle;

      ul {
        list-style-type: square;
      }
    }
  }
}

@mixin dark-diff-match-line {
  color: $dark-diff-match-bg;
  background: $dark-diff-match-color;
}

@mixin webkit-prefix($property, $value) {
  #{'-webkit-' + $property}: $value;
  #{$property}: $value;
}

/* http://phrappe.com/css/conditional-css-for-webkit-based-browsers/ */
@mixin on-webkit-only {
  @media screen and (-webkit-min-device-pixel-ratio:0) {
    @content;
  }
}

@mixin keyframes($animation-name) {
  @-webkit-keyframes #{$animation-name} {
    @content;
  }

  @keyframes #{$animation-name} {
    @content;
  }
}

@mixin include-keyframes($animation-name) {
  @include webkit-prefix(animation-name, $animation-name);
  @include keyframes($animation-name) {
    @content;
  }
}

/*
 * Mixin for status badges, as used for pipelines and commit signatures
 */
@mixin status-color($color-light, $color-main, $color-dark) {
  color: $color-main;
  border-color: $color-main;

  &:not(span):hover {
    background-color: $color-light;
    color: $color-dark;
    border-color: $color-dark;
  }
}

@mixin green-status-color {
  @include status-color($green-100, $green-500, $green-700);
}

@mixin fade($gradient-direction, $gradient-color) {
  visibility: hidden;
  opacity: 0;
  z-index: 2;
  position: absolute;
  bottom: 12px;
  width: 43px;
  height: 30px;
  transition-duration: .3s;
  -webkit-transform: translateZ(0);
  background: linear-gradient(to $gradient-direction, $gradient-color 45%, rgba($gradient-color, 0.4));

  &.scrolling {
    visibility: visible;
    opacity: 1;
    transition-duration: .3s;
  }

  .fa {
    position: relative;
    top: 5px;
    font-size: 18px;
  }
}

@mixin scrolling-links() {
  overflow-x: auto;
  overflow-y: hidden;
  -webkit-overflow-scrolling: touch;
  display: flex;
  flex-wrap: nowrap;

  &::-webkit-scrollbar {
    display: none;
  }
}

@mixin triangle($color, $border-color, $size, $border-size) {
  &::before,
  &::after {
    bottom: 100%;
    left: 50%;
    border: solid transparent;
    content: '';
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
  }

  &::before {
    border-color: transparent;
    border-bottom-color: $border-color;
    border-width: ($size + $border-size);
    margin-left: -($size + $border-size);
  }

  &::after {
    border-color: transparent;
    border-bottom-color: $color;
    border-width: $size;
    margin-left: -$size;
  }
}

/*
 * Mixin that fixes wrapping issues with long strings (e.g. URLs)
 *
 * Note: the width needs to be set for it to work in Firefox
 */
@mixin overflow-break-word {
  overflow-wrap: break-word;
  word-wrap: break-word;
  word-break: break-word;
  max-width: 100%;
}

/*
* Mixin that handles the container for the job logs (CI/CD and kubernetes pod logs)
*/
@mixin build-trace {
  background: $black;
  color: $gray-darkest;
  white-space: pre;
  overflow-x: auto;
  font-size: 12px;
  border-radius: 0;
  border: 0;
  padding: $grid-size;

  .bash {
    display: block;
  }

  &.build-trace-rounded {
    border-radius: $border-radius-base;
  }
}

@mixin build-trace-top-bar($height, $top_position) {
  height: $height;
  min-height: $height;
  background: $gray-light;
  border: 1px solid $border-color;
  color: $gl-text-color;
  position: sticky;
  position: -webkit-sticky;
  top: $top_position;
  padding: $grid-size;
}

/*
* Mixin that handles the position of the controls placed on the top bar
*/
@mixin build-controllers($control_font_size, $flex_direction, $with_grow, $flex_grow_size) {
  display: flex;
  font-size: $control_font_size;
  justify-content: $flex_direction;
  align-items: center;
  align-self: baseline;
  @if $with_grow {
    flex-grow: $flex_grow_size;
  }

  svg {
    width: 15px;
    height: 15px;
    display: block;
    fill: $gl-text-color;
  }

  .controllers-buttons {
    color: $gl-text-color;
    margin: 0 $grid-size;

    &:last-child {
      margin-right: 0;
    }
  }

  .btn-scroll.animate {
    .first-triangle {
      animation: blinking-scroll-button 1s ease infinite;
      animation-delay: 0.3s;
    }

    .second-triangle {
      animation: blinking-scroll-button 1s ease infinite;
      animation-delay: 0.2s;
    }

    .third-triangle {
      animation: blinking-scroll-button 1s ease infinite;
    }

    &:disabled {
      opacity: 1;
    }
  }

  .btn-scroll:disabled,
  .btn-refresh:disabled {
    opacity: 0.35;
    cursor: not-allowed;
  }
}

@mixin build-loader-animation() {
  position: relative;
  width: 6px;
  height: 6px;
  margin: auto auto 12px 2px;
  border-radius: 50%;
  animation: blinking-dots 1s linear infinite;
}

@mixin borderless-status-icon($color) {
  svg {
    border: 1px solid $color;
    border-radius: 50%;
  }
}
